import fileinput, argparse, itertools

def accumulate(times, line):
    row = []
    for time in line.split():
        row.append(float(time))
    times.append(row)

def average_latency(times):
    average_differences = []
    #print times
    for row in times:
        #print row
        average_differences.append( ( sum(row[1:]) / (len(row) - 1) ) - row[0] )
        print average_differences[len(average_differences) - 1]

    return sum(average_differences) / len(times)

def average_time(times):
    flattenedTimes = list(itertools.chain(*times))
    return sum(flattenedTimes)/len(flattenedTimes)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", nargs=1, help="the file to read in", type=str)
    parser.add_argument("-m", "--mode", nargs=1, help="use ")
    args = parser.parse_args()

    times = []
    for line in fileinput.input(args.file):
        accumulate(times, line)

    avg = 0.0
    if args.mode[0] == 'l':
        avg = average_latency(times);
    elif args.mode[0] == 't':
        avg = average_time(times)

    print "Average: %f" % avg

if __name__ == '__main__':
    main()